const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const validateRegisterInput = require('../validation/register');
const validateLoginInput = require('../validation/login');
const tokenValidation = require('../validation/tokenValidation');

const TopicModel = require('../schemas/topic');
const User = require('../schemas/user');

router.get('/topics/:page?', async (req, res) => {
    let page = req.params.page || 1;
    let size = 10;
    let start = (parseInt(page) - 1) * size;

    await TopicModel
        .find({})
        .sort({created: 'desc'})
        .skip(start)
        .limit(size)
        .exec(async (err, topics) => {
            await TopicModel.countDocuments({}, (err, count) => {
                res.json({
                    topics: topics,
                    current: Number(page),
                    pages: Math.ceil(count / 10)
                })
            })
        });
});

router.get('/topics/:page/:id/:messagePage', (req, res) => {
    let id = req.params.id;
    let messagePage = req.params.messagePage;
    let size = 5;
    let skip = (parseInt(messagePage) - 1) * size;
    TopicModel.findById(id, {messages: { $slice:[skip, size]}}, (err, messages) => {
        res.json({
            messages: messages.messages
        })
    });
});

// router.get('/topics/:page/:id', async (req, res) => {
//     let id = req.params.id;
//     let desiredTopic = await TopicModel.findById(id, (err, topic) => {
//     });
//     res.json({
//         messages: desiredTopic.messages
//     })
// });

router.post('/topics/:page', tokenValidation, async (req, res) => {
    let tokenSuccess = req.success;
    if (tokenSuccess) {
        let page = req.params.page || 1;
        let size = 10;
        let start = (parseInt(page) - 1) * size;

        if (req.body === {}) return res.sendStatus(400);
        const topicName = req.body['topicName'];
        let topic = new TopicModel({
            topicName: topicName
        });
        await topic.save({}, async (err, topic) => {
            if (err) console.log(err);
            console.log('here is the topic ', topic);
            await TopicModel
                .find({})
                .sort({created: 'desc'})
                .skip(start)
                .limit(size)
                .exec(async (err, topics) => {
                    await TopicModel.countDocuments({}, async (err, count) => {
                        await res.json({
                            topics: topics,
                            current: Number(page),
                            pages: Math.ceil(count / 10)
                        });
                    });
                });
        });
    } else {
        res.status(401).send({
            error: 'There is a problem with the token, login again please'
        })
    }
});


router.post('/topics/:page/:id', tokenValidation, async (req, res) => {
    let tokenSuccess = req.success;
    console.log('Is it success +', tokenSuccess);
    if (tokenSuccess) {
        let id = req.params.id;
        let text = req.body.text;
        let name = req.body.name;
        let message = {
            text: text,
            author: name
        };
        TopicModel.findOneAndUpdate(
            {_id: id},
            {
                $push: {
                    messages: message
                }
            },
            {new: true},
            (err, topic) => {
                if (err) console.log(err);
                console.log(topic);
                res.json({
                    messages: topic.messages
                })
            }
        )
    } else {
        res.status(401).send({
            error: 'There is a problem with the token, login again please'
        })
    }
});

router.post('/search', async (req, res) => {
    let value = req.body.value;
    TopicModel.find({
        topicName: {
            $regex: `${value}`
        }
    }, (err, topics) => {
        res.json({
            topics: topics
        });
    });
});

// REGISTRATION, LOGIN ROUTES

router.post('/register', (req, res) => {

    const {errors, isValid} = validateRegisterInput(req.body);

    if (!isValid) {
        return res.status(400).json(errors);
    }

    User.findOne({
        email: req.body.email
    }).then(user => {
        if (user) {
            return res.status(400).json({
                email: 'Account already exists'
            })
        } else {
            const avatar = gravatar.url(req.body.email, {
                s: '200',
                r: 'pg',
                d: 'mm'
            });
            const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password,
                avatar
            });

            bcrypt.genSalt(10, (err, salt) => {
                if (err) console.error('Error ', err);
                else {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if (err) console.error('Error ', err);
                        else {
                            newUser.password = hash;
                            newUser
                                .save()
                                .then(user => {
                                    res.json(user)
                                });
                        }
                    })
                }
            })
        }
    })
});

router.post('/login', (req, res) => {

    const {errors, isValid} = validateLoginInput(req.body);

    if (!isValid) {
        return res.status(400).json(errors);
    }

    const email = req.body.email;
    const password = req.body.password;

    User.findOne({email})
        .then(user => {
            if (!user) {
                errors.email = 'User not found';
                return res.status(404).json(errors);
            }
            bcrypt.compare(password, user.password)
                .then(isMatch => {
                    if (isMatch) {
                        const payload = {
                            id: user.id,
                            name: user.name,
                            avatar: user.avatar
                        };
                        jwt.sign(payload, 'secret', {
                            expiresIn: '1d'
                        }, (err, token) => {
                            if (err) console.error('Error with creating a token ', err);
                            else {
                                res.json({
                                    success: true,
                                    token: `Bearer ${token}`,
                                    name: user.name,
                                    avatar: user.avatar
                                });
                            }
                        })
                    } else {
                        errors.password = 'Incorrect password';
                        return res.status(400).json(errors);
                    }
                })
        })
});

router.get('/me', passport.authenticate('jwt', {session: false}), (req, res) => {
    return res.json({
        id: req.user.id,
        name: req.user.name,
        email: req.user.email
    });
});

module.exports = router;