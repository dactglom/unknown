var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.Promise = global.Promise;


var topic = new Schema(
    {
        topicName: 'String',
        messages: [{
            text: 'String',
            author: 'String',
            created: {
                type: Date,
                default: Date.now
            }
        }],
        created: {
            type: Date,
            default: Date.now
        }
    },
    {
        collection: 'topics'
    }
);

// const index = {topicName: 'text'};
// topic.index(index);

module.exports = mongoose.model('Topic', topic);