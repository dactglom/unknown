var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
var methodOverride = require('method-override');
var cors = require('cors');
var mongoose = require('mongoose');
var initDB = require('./db/init');

var app = express();
var route = require('./router/route');
app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cors('*'));
mongoose.connect(initDB.url, { useNewUrlParser: true });
app.use('/', route);

app.listen(process.env.PORT || 8081, () => console.log(`server started at 8081`));