const jwt = require('jsonwebtoken');

var myLogger = function (req, res, next) {

    const bearer = req.headers['authorization'];

    if (typeof bearer !== 'undefined') {

        jwt.verify(bearer, 'secret', (err, data) => {
            req.success = err ? false : true;
        });

        next();

    } else {
        req.success = false;
    }
};

module.exports = myLogger;