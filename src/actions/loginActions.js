import { createHeader } from "../helpers/requestHeaderHelpers";

export function login(email, password){
    return function(dispatch){
        dispatch(request());
        let myInit = {
            method: 'POST',
            headers: createHeader(localStorage.getItem('bearer')),
            mode: 'cors',
            cache: 'default',
            body: JSON.stringify({email: email, password: password}) 
        };
        fetch('/login', myInit)
            .then(response => response.json())
            .then(
                response => {
                    if(response.status !== 401){
                        localStorage.setItem('bearer', JSON.stringify(response));
                        localStorage.setItem('name', response.name.toUpperCase());
                        localStorage.setItem('avatar', response.avatar);
                        dispatch(requestSuccess(email));
                    } else {
                        dispatch(requestFailure(response.message));
                    }
                }
            )   
    }
}

export function request(){
    return { type: 'USER_LOGIN_REQUEST' }
}

export function requestSuccess(email){;
    return { type: 'USER_LOGIN_SUCCESS', email: email }
}

export function requestFailure(error){
    return { type: 'USER_LOGIN_FAILURE', error: error }
}
export function logout(){
    localStorage.removeItem('bearer');
    return { type: 'USER_LOGOUT' };
}