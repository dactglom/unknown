export function switchPage(page, totalPages) {
    return {
        type: 'SWITCH_PAGE',
        page: page,
        totalPages: totalPages
    }
}