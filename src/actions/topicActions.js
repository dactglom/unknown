export function messagesFetch(page, id){
    return function (dispatch) {
        dispatch(messageRequestStart());
        fetch(`/topics/${page}/${id}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: 'cors'
        }).then(data => data.json()).then(
            data => {
                dispatch(messageRequestSuccess(data.messages))
            }).catch(error => dispatch(messageRequestFailure(error)))
    }
}

export function messageRequestStart(){
    return { type: 'MESSAGES_REQUEST_START' };
}

export function messageRequestSuccess(messages){
    return { type: 'MESSAGES_REQUEST_SUCCESS', messages: messages }
}

export function messageRequestFailure(error){
    return { type: 'MESSAGES_REQUEST_FAILURE', error: error }
}