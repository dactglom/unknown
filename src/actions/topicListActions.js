import { switchPage } from "./pageActions";

export function topicListFetch(page = 1){
    return function (dispatch) {
        dispatch(taskRequestStart());
        fetch(`/topics/${page}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: 'cors'
        }).then(data => data.json()).then(
            data => {
                console.log(typeof data);
                console.log(data);
                dispatch(switchPage(data.current, data.pages));
                dispatch(taskRequestSuccess(data.topics))
            }).catch(error => dispatch(taskRequestFailure(error)))
    }
}

export function taskRequestStart(){
    return { type: 'TOPICS_REQUEST_START' };
}

export function taskRequestSuccess(topics){
    return { type: 'TOPICS_REQUEST_SUCCESS', topics: topics }
}

export function taskRequestFailure(error){
    return { type: 'TOPICS_REQUEST_FAILURE', error: error }
}