import React from 'react';
import { createHeader } from '../../helpers/requestHeaderHelpers';
import {messageRequestStart, messageRequestSuccess} from '../../actions/topicActions';
import { connect } from 'react-redux';

class AddNewMessageForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            text : ''
        };
        this.onChange = this.onChange.bind(this);
        this.addMessage = this.addMessage.bind(this);
        this.getInputRef = this.getInputRef.bind(this);
    }

    onChange(e){
        this.setState({ text : e.target.value });
        console.log(this.state.text);
        console.log(this.props.id);
    }

    getInputRef(node){
        this._Input = node;
    }

    addMessage(){
        this.props.refreshMessagesStart();
        let myInit = {
            method: 'POST',
            headers: createHeader(JSON.parse(localStorage.getItem('bearer')).token.split(' ')[1]),
            mode: 'cors',
            cache: 'default',
            body: JSON.stringify({text : this.state.text, name : localStorage.getItem('name')})
        };
        fetch(`/topics/${this.props.page}/${this.props.id}`, myInit)
            .then(data => data.json()).then(
            data => {
                this.props.refreshMessages(data.messages);
                console.log(data.messages)
            }).catch(error => console.log(error));
        this._Input.value = '';
    }

    render(){
        return (
            <div id="addNewMessageMessage-form-wrapper">
                <input
                type="text"
                ref={this.getInputRef}
                placeholder="Enter a message..."
                onChange={this.onChange}
                />
                <button type="button" onClick={this.addMessage}>ADD</button>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        refreshMessages: (message) => {
            dispatch(messageRequestSuccess(message))
        },
        refreshMessagesStart:() => {
            dispatch(messageRequestStart())
        }
    }
}

export default connect(null, mapDispatchToProps)(AddNewMessageForm);