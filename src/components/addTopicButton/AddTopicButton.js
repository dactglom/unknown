import React from 'react';
//import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import './addTopicButton.css';
import {createHeader} from "../../helpers/requestHeaderHelpers";
import {connect} from 'react-redux';
import { taskRequestSuccess, taskRequestStart } from "../../actions/topicListActions";
import {switchPage} from "../../actions/pageActions";

class AddTopicButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            toggle: false
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({
                toggle: false
            })
        }
    };

    setWrapperRef = (node) => {
        this.wrapperRef = node;
    };

    getInputRef = (node) => {
        this._input = node;
    };

    showPopup = () => {
        this.setState({
            toggle: !this.state.toggle
        })
    };

    onChange = (e) => {
        this.setState({
            title: e.target.value
        });
    };

    addTopic = () => {
        if(this.state.title) {
            this.props.refreshTopicsStart();
            fetch(`/topics/${this.props.page}`, {
                method: 'POST',
                headers: createHeader(JSON.parse(localStorage.getItem('bearer')).token.split(' ')[1]),
                mode: 'cors',
                cache: 'default',
                body: JSON.stringify({topicName: this.state.title})
            }).then(res => res.json()).then(data => {
                console.log(data.topics.length + ' LENGTH');
                this.props.refreshTopics(data.topics);
                this.props.refreshPages(data.current, data.pages);
            }).catch(err => console.log(err));
            this._input.value = '';
            this.setState({
                toggle: false,
                title: ''
            })
        }
    };

    render() {
        return (
            <div className="add-topic-button-wrapper" ref={this.setWrapperRef}>
                <div className="add-topic-button">
                    <p className={'add-topic-button-title'} onClick={this.showPopup}>
                        {'ADD TOPIC'}
                    </p>
                </div>
                {
                    this.state.toggle ?
                        <div className={'add-topic-modal'}>
                            <input
                                placeholder = {'Enter the topic name'}
                                onChange = {(e) => this.onChange(e)}
                                ref = {this.getInputRef}
                            />
                            <button onClick={this.addTopic}>{'ADD'}</button>
                        </div> : null
                }
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        page: state.pageReducer.page
    }
}

function mapDispatchToProps(dispatch){
    return {
        refreshTopics: (topics) => {
            dispatch(taskRequestSuccess(topics))
        },
        refreshTopicsStart: () => {
            dispatch(taskRequestStart())
        },
        refreshPages: (page, totalPages) => {
            dispatch(switchPage(page, totalPages))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTopicButton);