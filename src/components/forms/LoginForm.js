import React from 'react';
import { required, maxLengthPassword, customField, emailDataValidation, inputDataValidation } from "./RegistrationForm";
import { Field, reduxForm } from 'redux-form';
import { login } from '../../actions/loginActions';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.errorMessage = this.errorMessage.bind(this);
        this.showErrorMessage = this.showErrorMessage.bind(this);
    }
    handleSubmit(values) {
        this.props.dispatchLogin(values.email, values.password);
    }
    errorMessage(error){
       return <div className={'login-error-message'}>{error}</div>
    }
    showErrorMessage(props){
       this.errorMessage(props);
    }
    render() {
        const {error} = this.props;
        return (
                <div id="login-form-wrapper">
                    {this.props.redirect ? <Redirect from={'/login'} to={'/'} /> : null}
                    <form id={'login-form'} onSubmit={this.props.handleSubmit(this.handleSubmit)}>
                        <Field
                            name={'email'}
                            component={customField}
                            type={'text'}
                            placeholder={'Email'}
                            validate={[required, emailDataValidation]}
                        />
                        <Field
                            name={'password'}
                            component={customField}
                            type={'password'}
                            placeholder={'Password'}
                            validate={[required, maxLengthPassword, inputDataValidation]}
                        />
                        {error ? <div className={'login-error-message'}>{error}</div> : null}
                        <button type="submit" id={'login-button'}>Login</button>
                    </form>
                </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        redirect: state.loginReducer.redirect,
        error: state.loginReducer.error
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchLogin: (username, password) => {
            dispatch(login(username, password))
        }
    }
}

LoginForm = connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginForm);

export default reduxForm({
    form: 'login'
})(LoginForm);