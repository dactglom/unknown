import React from 'react'
import {Field, reduxForm} from 'redux-form';
import { Redirect } from 'react-router';
import { createHeader } from "../../helpers/requestHeaderHelpers";

// Important

// Validation functions
export const required = value => (value ? undefined : 'This field is required');
export const maxLength = value => (value.length > 30 || value.length < 5 ? 'Nickname must be in the range of 5 to 30 characters' : undefined);
export const maxLengthPassword = value => (value.length > 30 || value.length < 6 ? 'Password must be in range of 6 to 30 characters' : undefined);
export const inputDataValidation = value => (value.match(/^[a-zA-Z\d_]+$/) ? undefined : 'Invalid data');
export const emailDataValidation = value => (value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/) ? undefined : 'Invalid Email');

// Validation functions

// Custom Field

export const customField = ({ input, placeholder, type, meta: { touched, error } }) => (
    <div className={'login-input-wrapper'}>
            <input {...input} placeholder={placeholder} type={type}/>
            {touched && error ? <p>{error}</p> : null}
    </div>
);

class RegistrationForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            fireRedirect: false,
            error : ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(values){
        if (values.password !== values.confirm_password){
            this.setState({error: 'Passwords do not match'})    
        }
        let myInit = {
            method: 'POST',
            headers: createHeader(localStorage.getItem('bearer')),
            mode: 'cors',
            cache: 'default',
            body: JSON.stringify({name: values.username, email: values.email, password: values.password, password_confirm : values.confirm_password})
        };
        fetch('/register', myInit)
            .then(() => this.setState({ fireRedirect: true }))
            .catch((error) => console.log(error))
    };

    render(){
        return (
            <div id="registration-form-wrapper">
                <form id={'registration-form'} onSubmit={this.props.handleSubmit(this.handleSubmit)}>
                        <Field
                            name={'email'}
                            component={customField}
                            type={'text'}
                            placeholder={'Enter email'}
                            validate={[required, emailDataValidation]}
                        />
                        <Field
                            name={'username'}
                            component={customField}
                            type={'text'}
                            placeholder={'Nickname ( must contain 1 word )'}
                            validate={[required, maxLength, inputDataValidation]}
                        />
                        <Field
                            name={'password'}
                            component={customField}
                            type={'password'}
                            placeholder={'Password'}
                            validate={[required, maxLengthPassword, inputDataValidation]}
                        />
                        <Field
                            name={'confirm_password'}
                            component={customField}
                            type={'password'}
                            placeholder={'Confirm your password'}
                            validate={[required, maxLength, inputDataValidation]}
                        />
                    <button type="submit" id={'registration-button'}>Register</button>
                    { this.state.fireRedirect ? <Redirect from={'/registration'} to = {'/'} /> : null }
                    {this.state.error ? <p>{this.state.error}</p> : null}
                </form>
            </div>
        )
    }
}

RegistrationForm = reduxForm({
    form: 'registration'
})(RegistrationForm);

export default RegistrationForm;