import React, {Component} from 'react';
// import TopicList from "../topicList/TopicList";
import './greeting.css';
import { Link } from "react-router-dom";

class Greeting extends Component {
    render() {
        return (
            <div id = "greeting">
                <h1 className={'greeting-introduction'}>
                    {`Welcome to the website, stranger!`}
                </h1>
                <pre className={'code'}>
                    <code>
                        {`This website contains the most different topics to discuss. Users
share them hoping that someone will help them as soon as possible.
So you may be the one who they are looking for.`}
                    </code>
                </pre>
                <Link to={'/topics/1'} className={'greeting-button'}>{`Start exploring the topics!`}</Link>
            </div>
        );
    }
}

export default Greeting;
