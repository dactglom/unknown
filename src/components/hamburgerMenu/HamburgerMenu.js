import React from 'react';
import {Link} from "react-router-dom";
import { withRouter } from "react-router-dom";
import {connect} from 'react-redux';
import './hamburgerMenu.css';

class HamburgerMenu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            toggle: false
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({
                toggle: false
            });
        }
    };

    setWrapperRef = (node) => {
        this.wrapperRef = node;
    };

    toggleHamMenu = () => {
        this.setState({
            toggle: !this.state.toggle
        })
    };

    showLinks = () => {
        let url = this.props.location.pathname;
        console.log(url);
        let links = [
            {
                href: '/',
                title: 'Greeting',
                iconClass: 'far fa-hand-spock'
            },
            {
                href: '/topics',
                title: 'Topics',
                iconClass: 'fas fa-book'
            }
        ];
        return links.map((link, i, arr) => {
            return (
                <li
                    key={i}
                    className={'hamburger-link'}
                >
                    <Link to={link.href} onClick={this.toggleHamMenu}>
                        <i className={link.iconClass}/>
                        {link.title}
                        {
                            url === link.href || url === `${link.href}/${this.props.page}` ?
                                <span className="label label-error">Active</span> :
                                null
                        }
                    </Link>
                </li>
            )
        });
    };

    render() {
        const {toggle} = this.state;
        return (
            <div id="hamburger-wrapper">
                <i className="fas fa-bars" onClick={this.toggleHamMenu}/>

                <div
                    className={
                        toggle ?
                            'hamburger-menu opened' :
                            'hamburger-menu'
                    }
                    ref={this.setWrapperRef}
                >
                    <i className="fas fa-times" onClick={this.toggleHamMenu}/>
                    <ul className="hamburger-links-wrapper">
                        {/*<li className={'hamburger-link'}>*/}
                            {/*<Link to={'/'} onClick={this.toggleHamMenu}>*/}
                                {/*<i className="far fa-hand-spock"/>*/}
                                {/*{'Introduction'}*/}
                            {/*</Link>*/}
                        {/*</li>*/}
                        {/*<li className={'hamburger-link'}>*/}
                            {/*<Link to={'/topics/1'} onClick={this.toggleHamMenu}>*/}
                                {/*<i className="fas fa-book"/>*/}
                                {/*{'Topics'}*/}
                            {/*</Link>*/}
                        {/*</li>*/}
                        {this.showLinks()}
                    </ul>
                </div>

                {
                    toggle ?
                        <div id={'hamburger-shadow'}/> : null
                }

            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        page: state.pageReducer.page
    }
}

export default withRouter(connect(mapStateToProps)(HamburgerMenu));