import React from 'react';
import { Link } from "react-router-dom";
import './header.css';
import { connect } from 'react-redux';
import { logout } from '../../actions/loginActions';
import UserInfo from '../user/UserInfo';
import AddTopicButton from "../addTopicButton/AddTopicButton";
import SearchBar from "../searchBar/SearchBar";

class Header extends React.Component {

    render() {
        return (
            <header className="navbar navbar-main">
                <section className="navbar-section">
                    <UserInfo/>
                </section>
                <section className="navbar-center">
                    <Link to="/topics/1" className="navbar-brand mr-2 btn-navbar">UNKNOWN</Link>
                </section>
                <section className="navbar-section">
                    <div className="wrapper">
                        {this.props.jwt ?
                            <div className={'registered-navbar-buttons'}>
                                <SearchBar/>
                                <AddTopicButton/>
                                <button
                                    type="button" className="btn btn-link btn-navbar"
                                    onClick={this.props.logout}
                                >
                                    {'Log out'}
                                </button>
                            </div>
                             :
                            <div>
                                <Link to="/registration" className="btn btn-link btn-navbar">Sign Up</Link>
                                <Link to="/login" className="btn btn-link btn-navbar">Log In</Link>
                            </div>
                        }
                    </div>
                </section>
            </header>
        )
    }
}

function mapStateToProps(state) {
    return {
        jwt : state.loginReducer.jwt
    }
}

function mapDispatchToProps(dispatch) {
    return {
        logout: () => {
            dispatch(logout())
        }
    }
}

export default Header = connect(
    mapStateToProps, mapDispatchToProps
)(Header);