import React from 'react';
import {Link} from "react-router-dom";
import './pagination.css';

export default class Pagination extends React.Component {
    constructor(props) {
        super(props);
        this.createPaginationList = this.createPaginationList.bind(this);
    }

    createPaginationList() {
        let i = this.props.currentPage > 3 ? (this.props.currentPage - 2) : 1;
        const obj = [];
        if (i !== 1) {
            obj.push(
                <li key={0} className={'pagination-item disabled'}>
                    <span>...</span>
                </li>
            )
        }
        for (i; i <= this.props.totalPages && i <= this.props.currentPage + 2; i++) {
            obj.push(
                <li key={i} className={this.props.currentPage === i ? 'page-item active' : 'page-item'}>
                    <Link to={`/topics/${i}`}>{i}</Link>
                </li>
            );
            if (i === (this.props.currentPage + 2) && i < this.props.totalPages) {
                obj.push(
                    <li key={i+1} className="pagination-item disabled">
                        <span>...</span>
                    </li>
                )
            }
        }
        return obj;
    };

    render() {
        return (
            <ul className={'pagination'}>
                {this.createPaginationList()}
            </ul>
        )
    }
}