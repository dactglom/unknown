import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import Greeting from '../greeting/Greeting';
import Topic from '../topic/Topic';
import Header from '../header/Header';
import RegistrationForm from '../forms/RegistrationForm';
import LoginForm from '../forms/LoginForm';
import TopicList from "../topicList/TopicList";
import HamburgerMenu from "../hamburgerMenu/HamburgerMenu";

const Root = ({ store }) => (
    <Provider store={store}>
        <Router>
            <div id={'router-wrapper'}>
                <HamburgerMenu/>
                <Header/>
                <Switch>
                    <Route exact path="/" component = {Greeting} />
                    <Route exact path='/topics/:page/:id' component = {Topic}/>
                    <Route exact path='/topics/:page?' component = {TopicList}/>
                    <Route path="/registration" component = {RegistrationForm} />
                    <Route path="/login" component = {LoginForm} />
                </Switch>
            </div>
        </Router>
    </Provider>
);

export default Root;