import React from 'react';
import './searchBar.css';
import {connect} from 'react-redux';
import {createHeader} from "../../helpers/requestHeaderHelpers";
import TopicTitle from "../topicTitle/TopicTitle";


class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue: '',
            results: [],
            emptyResult: false
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({
                inputValue: '',
                results: [],
                emptyResult: false
            });
            console.log(this._inputRef);
            this._inputRef.value = '';
        }
    };

    setWrapperRef = (node) => {
        this.wrapperRef = node;
    };

    setInputRef = (node) => {
        this._inputRef = node;
    };

    getTopics = (value) => {
        this.setState({
            inputValue: value,
            emptyResult: false
        });
        if (!value) {
            this.setState({
                results: []
            })
        }
        if (value) {
            let myInit = {
                method: 'POST',
                headers: createHeader(JSON.parse(localStorage.getItem('bearer')).token.split(' ')[1]),
                mode: 'cors',
                cache: 'default',
                body: JSON.stringify({value: value})
            };
            fetch('/search', myInit)
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    if (data.topics.length === 0) {
                        this.setState({
                            emptyResult: true
                        })
                    }
                    this.setState({
                        results: data.topics
                    });
                    console.log(this.state.results);
                })
                .catch(err => console.log(err));
        }
    };

    showDesiredTopics = (value) => {
        return [...this.state.results].map((item, index) => {
            return <TopicTitle
                id={item._id}
                page={this.props.currentPage}
                key={index}
                title={item.topicName}
                search={true}
                searchBarText={value}
            />
        })
    };

    render() {
        const {inputValue, results, emptyResult} = this.state;
        return (
            <div className="input-group input-inline search-bar" ref={this.setWrapperRef}>
                <input
                    className="form-input"
                    type="text"
                    placeholder="Search for topics"
                    onChange={(e) => this.getTopics(e.target.value)}
                    ref = {this.setInputRef}
                />
                {
                    results ?
                        <ul className={'search-results-wrapper'}>
                            {
                                emptyResult ? <em>{`No topics found`}</em> : this.showDesiredTopics(inputValue)
                            }
                        </ul> : null
                }
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        currentPage: state.pageReducer.page
    }
}

export default connect(mapStateToProps, null)(SearchBar);

// EXAMPLE OF USING 'useState' introduced in React 16.8 (don't forget to import it from 'react' package)

// function SearchBar(){
//     const [string, changeString] = useState('');
//     return(
//         <div className="input-group input-inline search-bar">
//             <input
//                 className="form-input"
//                 type="text"
//                 placeholder="search"
//                 onChange={(e) => changeString(e.target.value)}
//             />
//         </div>
//     )
// }