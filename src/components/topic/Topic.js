import React from 'react';
import {connect} from 'react-redux';
import {messagesFetch} from "../../actions/topicActions";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import './topic.css';
import AddNewMessage from "../addNewMessageForm/AddNewMessageForm";

class Topic extends React.Component {

    componentDidMount() {
        const {page, id} = this.props.ownProps.match.params;
        console.log(page + ' ' + id);
        this.props.getTopicMessages(page, id);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.ownProps.match.params !== prevProps.ownProps.match.params){
            this.props.getTopicMessages(this.props.ownProps.match.params.page, this.props.ownProps.match.params.id);
        }
    }

    render() {
        const messageList = [...this.props.messages].map((message, id) => {
            return <p key={id}>{message.text}</p>
        });
        const emptyMessageList = (
            <div className={"empty-messages-section"}>
                <span className={'label no-messages-label'}>There are no messages yet!</span>
                <FontAwesomeIcon icon={'inbox'}/>
            </div>
        );
        return (
            <div className={'message-list'}>
                {
                    this.props.fetching ?
                        <div className={'loading loading-lg'}/> :
                        this.props.messages.length !== 0 ? messageList : emptyMessageList
                }
                <AddNewMessage page = {this.props.page} id = {this.props.ownProps.match.params.id}/>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        messages: state.topicReducer.messages,
        ownProps: ownProps,
        fetching: state.topicReducer.fetching,
        page: state.pageReducer.page
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getTopicMessages: (page,id) => {
            dispatch(messagesFetch(page,id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Topic);