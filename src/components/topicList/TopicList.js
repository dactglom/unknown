import React from 'react';
import {connect} from 'react-redux';
//import {addTopic} from "../../actions/topicMockActions";
import {topicListFetch} from "../../actions/topicListActions";
import './topicList.css';
import TopicTitle from "../topicTitle/TopicTitle";
import Pagination from "../pagination/Pagination";

class TopicList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: ''
        }
    }

    componentDidMount() {
        this.props.getTopicList(this.props.ownProps.match.params.page);
        console.log(this.props.ownProps.match.params.page);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.topics !== prevProps.topics) {
            this.showTopics();
        }
        if(this.props.ownProps.match.params.page !== prevProps.ownProps.match.params.page ){
            this.props.getTopicList(this.props.ownProps.match.params.page);
        }
    }

    showTopics = () => {
        return [...this.props.topics].map((item, index) => {
            return <TopicTitle id={item._id} page = {this.props.currentPage} key={index} title={item.topicName}/>
        })
    };

    render() {
        const { currentPage, totalPages } = this.props;
        return (
            <div className={'topic-list'}>
                <mark className={'topic-list-title'}>{`Topics`}</mark>
                {
                    this.props.fetching ?
                        <div className={'loading loading-lg'}/> :
                        this.showTopics()
                }
                <Pagination currentPage = {currentPage} totalPages = {totalPages} />
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        topics: state.topicListReducer.topics,
        fetching: state.topicListReducer.fetching,
        currentPage: state.pageReducer.page,
        totalPages: state.pageReducer.totalPages,
        ownProps: ownProps
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getTopicList: (page) => {
            dispatch(topicListFetch(page))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TopicList);