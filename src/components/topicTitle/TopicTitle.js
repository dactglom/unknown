import React from 'react';
import {Link} from "react-router-dom";
import './topicTitle.css';

const TopicTitle = ({title, page, id, search, searchBarText}) => {
    function showHighlighted(text, highlight) {
        let parts = text.split(new RegExp(`(${highlight})`, 'gi'));
        return <Link to={`/topics/${page}/${id}`}>
            {
                parts.map((part, i) =>
                    <span key={i} style={part === highlight ? {fontWeight: 'bold'} : {}}>
                        {part}
                    </span>
                )
            }
        </Link>;
    }

    return (
        <div className={`topic-title topic-${title} c-hand`}>
            {
                search ? showHighlighted(title, searchBarText) : <Link to={`/topics/${page}/${id}`}>{title}</Link>
            }
        </div>
    )
};

export default TopicTitle;