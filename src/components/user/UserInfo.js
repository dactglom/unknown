import React from 'react';
import { connect } from 'react-redux';
import './userInfo.css';

const UserInfo = ({jwt}) =>(
	<div className= 'user-info-wrapper'>
		{
			jwt && localStorage.getItem('name') ?
			<div className="registered">
			<figure className="avatar avatar-lg">
                <img src={`${localStorage.avatar}`} alt={'registered-user-logo'}/>
            </figure>
            <p className="username"> {`${localStorage.getItem('name')}`}</p> 
            </div>: null 
		}
	</div>
);

function mapStateToProps(state){
    return{
        jwt: state.loginReducer.jwt
    }
}

export default connect(mapStateToProps)(UserInfo);