export const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
};

export const createHeader = (bearer) => {
    return {...headers, Authorization: bearer}
};


export function bearerFreshness(successFn, failureFn) {
    const bearer = JSON.parse(localStorage.getItem('bearer'));
    if (bearer.access && (bearer.expires < Date.now() / 1000)) {
        successFn();
    } else {
        refreshBearerRequest(successFn,failureFn);
    }

}

export function refreshBearerRequest(successfulFn, failedFn) {
    return fetch('/validation', {
        method: "POST",
        headers: createHeader(localStorage.getItem('bearer')),
        mode: 'cors'
    })
        .then(response => response.json())
        .then(bearer => {
            localStorage.setItem('bearer', JSON.stringify(bearer));
            successfulFn();
        })
        .catch(error =>
            error.status === 401 ? failedFn() : null
        )
}