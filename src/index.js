import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import {createStore, applyMiddleware} from 'redux';
import generalReducer from './reducers/index';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import Root from "./components/routing/Root";
//import '../node_modules/spectre.css/dist/spectre.min.css'
import './index.css';

const store = createStore(
    generalReducer,
    composeWithDevTools(applyMiddleware(thunk))
);

ReactDOM.render(
    <Root store={store}/>,
    document.getElementById('root')
);

// If you want your greeting to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
