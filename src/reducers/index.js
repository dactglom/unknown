	
import topicListReducer from './topicListReducer';
//import topicMockReducer from './topicMockReducer';
import topicReducer from './topicReducer';
import loginReducer from './loginReducer';
import pageReducer from './pageReducer';
import { library } from '@fortawesome/fontawesome-svg-core';
import {faInbox, faPlusCircle} from '@fortawesome/free-solid-svg-icons';
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

library.add(faInbox, faPlusCircle);

export default combineReducers({
    // topicMockReducer: topicMockReducer
    topicListReducer: topicListReducer,
    loginReducer: loginReducer,
    topicReducer: topicReducer,
    pageReducer: pageReducer,
    form: formReducer
});