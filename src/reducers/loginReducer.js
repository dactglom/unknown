const initialState = {
    loggingIn: false,
    jwt: !!localStorage.bearer,
    email: '',
    error: '',
    redirect: false
}

export default function authentication(state = initialState, action){
    switch(action.type){
        case 'USER_LOGIN_REQUEST':
            return {
                ...state,
                loggingIn: true,
                redirect: false
            };
        case 'USER_LOGIN_SUCCESS':
            return {
                ...state,
                loggingIn: false,
                jwt: !!localStorage.bearer,
                email: action.email,
                redirect: true
            };
        case 'USER_LOGIN_FAILURE':
            return {
                ...state,
                loggingIn: false,
                jwt: !!localStorage.bearer,
                error: action.error,
                redirect: false
            };
        case 'USER_LOGOUT':
            return {
                ...state,
                jwt: !!localStorage.bearer,                                   
                username: '',
                redirect: false
            };
        default:
            return state            
    }
}