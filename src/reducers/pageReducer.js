const initialState = {
    page: 1,
    totalPages: null
};

export default function pageReducer(state = initialState, action) {
    switch (action.type) {
        case 'SWITCH_PAGE':
            return {
                page: action.page,
                totalPages: action.totalPages
            };
        default:
            return state
    }
}