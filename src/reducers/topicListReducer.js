const initialState = {
    topics: [],
    fetching: false,
    successfully: false,
    error: null
};

export default function topicListReducer(state = initialState, action) {
    switch (action.type) {
        case 'TOPICS_REQUEST_START':
            return {
                ...state,
                fetching: true
            };
        case 'TOPICS_REQUEST_SUCCESS':
            return {
                ...state,
                fetching: false,
                topics: action.topics,
                successfully: true,
                error: null
            };
        case 'TOPICS_REQUEST_FAILURE':
            return {
                ...state,
                fetching: false,
                successfully: false,
                error: action.error
            };
        default: return state
    }
}