const initialState = {
    topics: [
        {
            title: 'Task 1',
            messages: [
                'Number 1',
                'Number 2',
                'Number 3'
            ]
        },
        {
            title: 'Task 2',
            messages: [
                'Number 4',
                'Number 5',
                'Number 6'
            ]
        },
        {
            title: 'Task 3',
            messages: [
                'Number 7',
                'Number 8',
                'Number 9'
            ]
        }
    ]
};

export default function topicMockReducer(state = initialState, action) {
    switch (action.type) {
        case 'ADD_TOPIC':
            return {
                ...state,
                topics: state.topics.concat({ title: action.title, messages: [] })
            };
        default:
            return state;
    }
}