const initialState = {
    messages: [],
    fetching: false,
    successfully: false,
    error: null
};

export default function topicReducer(state = initialState, action) {
    switch (action.type) {
        case 'MESSAGES_REQUEST_START':
            return {
                ...state,
                fetching: true
            };
        case 'MESSAGES_REQUEST_SUCCESS':
            return {
                ...state,
                fetching: false,
                messages: action.messages,
                successfully: true,
                error: null
            };
        case 'MESSAGES_REQUEST_FAILURE':
            return {
                ...state,
                fetching: false,
                successfully: false,
                error: action.error
            };
        default: return state
    }
}